# RNA-Seq analysis of transcription in Arabidopsis pollen

This repository containing code and data from a 2013 paper titled
"RNA-seq of Arabidopsis pollen uncovers novel transcription and
alternative splicing" (see:
http://www.ncbi.nlm.nih.gov/pubmed/23590974) 

We wrote the paper before we started using bitbucket for version-control. 
Instead, we were using Dropbox.

This means: everything here was added post-publication. 

Also, some things were added in subsequent years to support new 
research efforts.

* * *

# What's here

Each module runs as a mostly self-contained project in RStudio. 
As such, each folder contains an ".Rproj" file. To run the code in RStudio,
open the .Rproj file and go from there.

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are version-controlled
in ExternalDataSets but may also be available from external sites.

Analysis modules and other directories contain:

* .Rmd files (RMarkdown)
* Output from knitting Rmd files in HTML (Web pages)
* Folders with results from running .Rmd files
* Folders with externally supplied data used only in one module (data)

Results folders also contain high-quality image files suitable for inclusion
in slides or publications.

* * *


# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu

# License

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT