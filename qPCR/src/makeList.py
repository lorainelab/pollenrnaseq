#!/usr/bin/env python

"""Identify genes for qPCR testing by reading file names.
Retrieve data for each gene from RPKM and counts files.
Run in same directory where this file resides.
"""

import os,sys,re,gzip

def main():
    reg=re.compile('(AT[1-5CM]G\d{5})')
    fs=os.listdir('../IGB-pics')
    genes = []
    for f in fs:
        gene=f.split('.')[0]
        match=reg.match(gene)
        if match:
            genes.append(match.groups()[0])
    raw_counts='../../Counts/Original/pollen_vs_seedling_raw_counts.txt.gz'
    rpkm_counts='../../Counts/Revised/new_pollen_vs_seedling_RPKM.tsv.gz'
    d = {}
    fh = gzip.open(raw_counts,'rb')
    line=fh.readline()
    line=fh.readline()
    # 0: AGI 
    # 1: gene.type
    # 2: symbol
    # 3: descr
    # 4: unique.ps
    # 5: promiscuous.ps
    # 6: pollen
    # seedling1	seedling2	Ave.seedling
    # sys.stderr.write(line)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        agi=toks[0]
        if agi in genes:
            # agi, symbol, descr, ps, ps, pollen, seedling
            d[agi]=[agi,toks[3],toks[4],toks[5],toks[6]]
    fh = gzip.open(rpkm_counts,'rb')
    fh.readline()
    while 1:
        line = fh.readline()
        if line.startswith('#'):
            continue
        if not line:
            fh.close()
            break
        toks = line.rstrip().split('\t')
        agi = toks[0]
        if agi in genes:
            lst=d[agi]
            lst.append(toks[6])
    heads=['AGI','descr','unique.ps','promiscuous.ps','counts.pollen',
           'rpkm.pollen']
    sys.stdout.write('\t'.join(heads)+'\n')
    for gene in genes:
        toks=d[gene]
        sys.stdout.write('\t'.join(toks)+'\n')
        

if __name__ == '__main__':
    main()
