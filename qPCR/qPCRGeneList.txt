AGI	descr	unique.ps	promiscuous.ps	counts.pollen	rpkm.pollen
AT1G07360	CCCH-type zinc fingerfamily protein with RNA-binding domain	261082_at	None	404.0000	5.6073
AT2G17010	Mechanosensitive ion channel family protein	None	None	3340.0000	32.5699
AT2G28390	SAND family protein	265256_at	None	121.0000	1.3072
AT2G39805	Integral membrane Yip1 family protein	None	None	1013.0000	17.8149
AT3G10290	Nucleotide-sugar transporter family protein	None	None	500.0000	9.7677
AT3G58480	calmodulin-binding family protein	None	None	10253.0000	128.2584
AT4G11845	Interleukin-1 receptor-associated kinase 4 protein	None	None	23.0000	0.7478
AT4G29460	Phospholipase A2 family protein	None	None	5974.0000	184.0284
AT4G32510	HCO3- transporter family	None	None	58686.0000	680.3897
AT4G34270	TIP41-like family protein	253287_at	None	7.0000	0.1309
AT5G02400	pol-like 2	None	None	51.0000	0.5904
AT5G45740	Ubiquitin domain-containing protein	None	None	23.0000	1.5103
AT5G49920	Octicosapeptide/Phox/Bem1p family protein	None	None	70253.0000	1131.8952
