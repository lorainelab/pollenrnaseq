#!/bin/bash

# run like this: 
# qsub-tophat.sh 1>jobs.out 2>jobs.err
# to kill jobs:
# cat jobs.out | xargs qdel 

SCRIPT=tophat.sh
G=A_thaliana_Jun_2009
O=SRP022162

pollen="SRR847501_1.fastq.gz,SRR847502_1.fastq.gz"
leaf1="SRR847503_1.fastq.gz,SRR847504_1.fastq.gz"
leaf2="SRR847505_1.fastq.gz,SRR847506_1.fastq.gz"

S="pollen"
CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G,F=$pollen $SCRIPT"
if [ ! -s $O/$S/accepted_hits.bam ];
then
    $CMD
fi

S="leaf1"
CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G,F=$leaf1 $SCRIPT"
if [ ! -s $O/$S/accepted_hits.bam ];
then
    $CMD
fi

S="leaf2"
CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G,F=$leaf2 $SCRIPT"
if [ ! -s $O/$S/accepted_hits.bam ];
then
    $CMD
fi




