#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=8:00:00
cd $PBS_O_WORKDIR
module load bowtie2
module load tophat
# defined by qsub -v: S, G, O, F
# O, S, G defined by qsub -v 
tophat -p 8 -I 5000 -o $O/$S $G $F

