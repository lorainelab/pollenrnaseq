#!/bin/bash

# run like this: 
# qsub-fastq-dump.sh 1>jobs.out 2>jobs.err
# to kill jobs:
# cat jobs.out | xargs qdel 

SCRIPT=fastq-dump.sh

FS=`ls *.sra`
for F in $FS
do
    S=`basename $F .sra`
    if [ ! -s $S.fastq.gz ];
    then
	CMD="qsub -N $S -o $S.out -e $S.err -vF=$F $SCRIPT"
	$CMD
    fi
done


