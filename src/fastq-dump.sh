#!/bin/bash
#PBS -q copperhead
#PBS -l walltime=01:30:00
#PBS -l nodes=1:ppn=8
#PBS -l mem=16gb

module load sra-tools
cd $PBS_O_WORKDIR
fastq-dump --split-files --gzip $F 

