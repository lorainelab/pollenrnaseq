# Supplemental Data Files and Corrections

This folder contains original and revised Supplemental Data Files from
the the article titled [High-throughput sequencing of Arabidopsis
thaliana pollen cDNA uncovers novel transcription and alternative
splicing[(http://www.ncbi.nlm.nih.gov/pubmed/23590974) published in
Plant Physiology in June, 2013.

After the paper was published, we noticed two errors in how the
Supplemental Data Tables II and III were generated. We corrected the
errors and here provide copies of the original and corrected data
files. We also provide an R Markdown (.Rmd) document describing the
errors and showing how we validated the new files. To view and/or run
the file, first look at SupplementalDataFiles1to3.html and follow the
instructions on how to run the the .Rmd file using RStudio/knitr.

